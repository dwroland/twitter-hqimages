# Twitter HQImages Userscript

## Description
This is a **UserScript** designed to enhance the Twitter (X) experience by allowing users to open and view high-quality images. It modifies image URLs to request the highest available resolution (4096x4096) and optionally forces PNG format.

## Features
- Automatically updates image URLs to request the highest resolution.
- Works on `twitter.com`, `x.com`, and `pbs.twimg.com`.
- Enables quick opening of high-quality images via **right-click** (context menu).
- Supports automatic opening of images in a new tab.

## Installation
This script is intended to be used with the **Tampermonkey** browser extension.

### Steps:
1. Install the Tampermonkey browser extension.
2. Create a new userscript in Tampermonkey.
3. Copy and paste the provided script into the editor.
4. Save and enable the script.

## License
This script is provided under the MIT License.