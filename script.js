// ==UserScript==
// @name         Twitter HQImages
// @version      1.8
// @author       Greust
// @match        http*://pbs.twimg.com/media/*
// @match        http*://mobile.twitter.com/*
// @match        http*://twitter.com/*
// @match        http*://x.com/*
// @grant        GM_openInTab
// @run-at       document-start
// @updateURL    https://gitlab.com/dwroland/twitter-hqimages/-/raw/master/script.js
// @downloadURL  https://gitlab.com/dwroland/twitter-hqimages/-/raw/master/script.js
// @require      http://code.jquery.com/jquery-3.4.1.min.js
// ==/UserScript==
/* global $ */

const FORCE_PNG = false;
const IMG_SIZE = "4096x4096";
const ENABLE_QUICK_OPEN = true;

function add_params(params) {
    let param_string = "?";
    for (let param in params) {
        param_string += param + "=" + params[param] + "&";
    }
    return param_string.substring(0, param_string.length - 1);
}

function url_builder() {
    let params = {};
    if (FORCE_PNG) params.format = "png";
    params.name = IMG_SIZE;
    let newURL = window.location.protocol + "//"
        + window.location.host
        + window.location.pathname
        + add_params(params)
        + window.location.hash
        ;
    return newURL;
}

function url_builder_ppm(ev) {
    let params = {};
    if (FORCE_PNG) params.format = "png";
    else params.format = new URL(ev.target.src).searchParams.get("format")
    params.name = IMG_SIZE;
    let newURL = ev.target.src.split("?")[0]
        + add_params(params)
        + window.location.hash
        ;
    return newURL;
}

(function () {
    'use strict';
    window.addEventListener("load", function (ev) {
        if (ENABLE_QUICK_OPEN && (window.location.host.match(/twitter\.com/) || window.location.host.match(/x\.com/))) {
            document.addEventListener("contextmenu", function (ev) {
                if (ev.target.tagName == "IMG") {
                    let new_url = url_builder_ppm(ev)
                    GM_openInTab(new_url, true);
                    ev.preventDefault();
                    ev.stopPropagation();
                    return false;
                }
            })
        }
    })
})();